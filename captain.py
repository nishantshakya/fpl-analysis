
import pandas as pd
import utils
# import sys
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (1,19)
MIN_DIFFICULTY = 2
#teams = [11,10]
def gameweek_filter(df, home_away):
  df = df.query( f"event >={GW_START} & event <={GW_END} & {home_away}_difficulty <= {MIN_DIFFICULTY} & {home_away} == {FPLlib.CAPTAINS} ")
  return df

def getGameweeks(data, team):
  if team in data:
    return data[team]
  else:
    return []

df = pd.DataFrame(FPLlib.getFPLData())
filtered_data_home = gameweek_filter(df,'team_h')
filtered_data_away = gameweek_filter(df,'team_a')
# easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('event')['team_h'].apply(list)
# easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('event')['team_a'].apply(list)

easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('team_h')['event'].apply(list)
easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('team_a')['event'].apply(list)

for i in FPLlib.CAPTAINS:
  all_fix = easy_gws_away.loc[i]+easy_gws_home.loc[i]
  print(i,all_fix)
  # if len(all_fix) == GW_END - GW_START + 1:
  #   print(i)
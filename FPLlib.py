import json
import requests

TEAM_INDICES = {
  "1": "Arsenal",
  "2":"Aston Villa",
  "3":"Bournemouth",
  "4":"Brighton",
  "5":"Burnley",
  "6":"Chelsea",
  "7":"Crystal Palace",
  "8":"Everton",
  "9":"Leicester",
  "10":"Liverpool",
  "11":"Manchester City",
  "12":"Manchester United",
  "13":"Newcastle",
  "14":"Norwich",
  "15":"Sheffield United",
  "16":"Southampton",
  "17":"Tottenham",
  "18":"Watford",
  "19":"West Ham",
  "20":"Wolves"
}

DEF_DEFENDERS = [6, 8,9,10,11,12,17]
EXPENSIVE_DEFENDERS = [6,8,10,11,12,17,20]
EXPENSIVE_GKS = [1, 6,7, 8, 9,10,11,12,13,17,18,19,20]
POOR_ATTACKS = [5,7]
ATTACKING_TEAMS = [1,3,4,6,8,9,10,11,12,14,15,17,18,19,20]
CAPTAINS = [1,10,11,17]
CHEAP_DEF = [1,4,5,6,8,9,10,11,12,13,15,16,17,20]
API_URL = "https://fantasy.premierleague.com/api/fixtures/"
def getFPLData():
  response = requests.get(API_URL)
  data = json.loads(response.text)
  return data